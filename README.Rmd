---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "60%",
  fig.align = "center"
)
```

# <img src='man/figures/epitheme.png' align="right" height="120" />epitheme

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![pipeline status](https://gitlab.com/epi-uk/epitheme/badges/master/pipeline.svg)](https://gitlab.com/epi-uk/epitheme/-/commits/master)
[![coverage report](https://gitlab.com/epi-uk/epitheme/badges/master/coverage.svg)](https://gitlab.com/epi-uk/epitheme/-/commits/master)
<!-- badges: end -->

R package to theme EPI charts created using `ggplot2`. [Read the documentation here](https://epi-uk.gitlab.io/epitheme/) to learn how to install and use it.

## Installation

The easiest way to install the latest version of the package is to run the following at an R terminal:

```{r, eval = FALSE}
remotes::install_gitlab("epi-uk/epitheme")
```

You may need to first install the `remotes` packages with `utils::install.packages("remotes")`

If you are developing the `{epitheme}` code base then you should install on your local machine. To do that, clone the repository and install using `devtools` or the built-in RStudio commands.

## Usage

This is a quick start guide to using the package. For more details, see the vignette, `vignette("using_epitheme")`.

The theme can be applied to either a single chart or all charts.

### A default ggplot chart

```{r}
library(ggplot2)

# Sample data
data(mpg)

# ggplot scatter
plot <- ggplot(
  mpg,
  aes(
    x = displ,
    y = cty,
    colour = cyl
  )
) +
  geom_point()

plot
```

### Single chart

To apply the theme to a single chart, you can call it on a ggplot chart explicitly using `::`. For example:

```{r}
plot +
  # Use the EPI theme
  epitheme::theme_epi() +
  # Use the colour scales exported from epitheme
  epitheme::scale_colour_continuous()
```

### All charts

Load the `epitheme` package using `library(epitheme)` instead to apply to all charts. Note this overwrites some `{ggplot2}` functions.

```{r}
library(epitheme)

plot
```
