# epitheme 0.4.0

## Major changes

- Tweaks to the default theme to more closely follow the Excel chart theme.
- Add a theme for maps - `theme_epi(map = TRUE)` - which strips off axes and other unnecessary chart components.
- Add `scale_fill_continuous()` to create colour gradients, which are particularly useful for colouring a map using a continuous variable, eg percentage FSM.
- New vignette demonstrating the use of these functions in `sf` maps.
- Function `scale_fill_epi` removed in favour of `scale_fill_discrete` and `scale_fill_continuous`.
- `epitheme` now masks `ggplot2::scale_*` functions, so you can use `scale_*` without needing to specify the EPI version.
- Add an `.onAttach`  so when `epitheme` is loaded (`library(epitheme)`) the EPI theme and colour palette are used by default when calling `ggplot2::ggplot()`.
- Added sequential colour palettes of both 6 and 10 colours.

## Development changes

- Use `precommit` framework to enforce pre-commit hooks.
