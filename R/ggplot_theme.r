#' Apply EPI theme to ggplot2 chart.
#'
#' @param colour_foreground The colour of foreground elements
#' @param colour_background The colour of background elements
#' @param colour_shading The colour of shaded elements
#' @param map logical, implement map styling
#' @param ... Additional arguments to be passed to ggplot2::theme_minimal()
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   theme_epi()
#' }
#' @importFrom ggplot2 element_text theme_minimal theme element_blank element_line element_rect element_text rel
#' @export
theme_epi <- function(colour_foreground = epi_foreground,
                      colour_background = epi_background,
                      colour_shading = epi_shading,
                      map = FALSE,
                      ...) {
  # Text properties
  font <- "source_sans_pro"
  txt <- ggplot2::element_text(
    family = font,
    size = 11,
    colour = colour_foreground,
    face = "plain"
  )
  bold_txt <- ggplot2::element_text(
    family = font,
    size = 11,
    colour = colour_foreground,
    face = "bold"
  )
  title_txt <- ggplot2::element_text(
    family = font,
    size = 24,
    colour = colour_foreground,
    face = "bold"
  )

  # Chart elements
  out <- theme_minimal(...) +
    theme(
      # Clean up
      legend.key = element_blank(),
      panel.grid.minor = element_blank(),
      panel.grid.major = element_blank(),
      panel.border = element_blank(),

      # Text basics
      text = txt,
      plot.title = title_txt,
      axis.title = bold_txt,
      axis.text = txt,
      legend.title = bold_txt,
      legend.text = txt,

      # Axes
      axis.line = element_line(colour = colour_shading),
      axis.ticks = element_line(colour = colour_shading),

      # Legend
      legend.position = "bottom",

      # Faceting
      panel.background = element_blank(),
      panel.grid = element_line(colour = colour_shading),
      panel.grid.major.y = element_blank(),
      strip.background = element_rect(
        fill = colour_shading,
        color = colour_shading
      ),
      strip.text = element_text(
        size = 11,
        color = colour_foreground,
        face = "bold"
      )
    )

  # Maps
  if (map == TRUE) {
    out <- out +
      theme(
        axis.line = element_blank(),
        axis.text = element_blank(),
        axis.ticks = element_blank(),
        axis.title = element_blank()
      )
  }

  out
}

#' Apply EPI theme to ggplot2 maps.
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by [epitheme::theme_epi())]
#' which now has a simple `map` argument.
#' It will be removed in the next version.
#'
#' @param colour_foreground The colour of foreground elements
#' @param colour_background The colour of background elements
#' @param ... Additional arguments passed to theme
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   geom_sf() +
#'   theme_map_epi()
#' }
#'
#' @importFrom ggplot2 theme element_blank
#'
#' @export
theme_map_epi <- function(colour_foreground = epi_foreground,
                          colour_background = epi_background,
                          ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "theme_map_epi()",
    "theme_epi(map)"
  )
  epitheme::theme_epi(
    colour_foreground,
    colour_background
  ) +
    theme(
      axis.line = element_blank(),
      axis.text.x = element_blank(),
      axis.text.y = element_blank(),
      axis.ticks = element_blank(),
      axis.title.x = element_blank(),
      axis.title.y = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank(),
      ...
    )
}
