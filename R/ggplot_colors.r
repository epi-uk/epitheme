# Functions to create EPI colour scales
# Draws heavily on https://www.r-bloggers.com/how-to-create-a-ggplot-theme-unicorn-edition/

library(ggplot2)

# chart colours
epi_foreground <- "grey30"
epi_background <- "grey50"
epi_shading <- "grey87"
epi_rect_fill <- "#bdbdbd"

#' EPI corporate colour
#' @examples
#' scales::show_col(epi_corp_colour)
#' @export
epi_corp_colour <- "#11a08a"

#' Categorical colour palette for EPI charts
#' @examples
#' scales::show_col(epi_palette_discrete)
#' @export
epi_palette_discrete <- c(
  "#11a08a",
  "#BC0050",
  "#FFBD00",
  "#4A14FF",
  "#095045",
  "#FF78FF",
  "#F68F54",
  "#0063E6",
  "#C4E7E2"
)

#' Sequential 6-colour palette for EPI charts
#' @examples
#' scales::show_col(epi_palette_seq6)
#' @export
epi_palette_seq6 <- c(
  "#095045",
  "#0D7868",
  "#11A08A",
  "#4DB8A7",
  "#88D0C5",
  "#C4E7E2"
)

#' Sequential 10-colour palette for EPI charts
#' @examples
#' scales::show_col(epi_palette_seq10)
#' @export
epi_palette_seq10 <- c(
  "#074037",
  "#09584C",
  "#0C7061",
  "#0E8875",
  "#11A08A",
  "#35AE9C",
  "#58BDAD",
  "#7CCBBF",
  "#A0D9D0",
  "#C4E7E2"
)

#' Colour scale to fill continuous geoms using EPI colours
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by the simpler [epitheme::scale_fill_continuous())]
#' which can be used in the same fashion. It will be removed in the next version.
#'
#' @param colour_low Bottom of colour gradient
#' @param colour_high Top of colour gradient
#' @param ... Arguments passed to scale_fill_gradient
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   scale_fill_continuous_epi()
#' }
#' @seealso [scale_colour_discrete_epi()], [scale_colour_continuous_epi()]
#'
#' @importFrom ggplot2 scale_fill_gradient
#' @export
scale_fill_continuous_epi <- function(colour_low = "#F5F5F5",
                                      colour_high = epi_corp_colour,
                                      ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "scale_fill_continuous_epi()",
    "scale_fill_continuous()"
  )
  structure(list(
    scale_fill_gradient(
      low = colour_low,
      high = colour_high,
      ...
    )
  ))
}

#' Colour scale to fill discrete geoms using EPI colours
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by the simpler [epitheme::scale_fill_discrete())]
#' which can be used in the same fashion. It will be removed in the next version.
#'
#' @param palette A list of colours to use
#' @param ... Arguments passed to scale_fill_manual
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   scale_fill_discrete_epi()
#' }
#' @seealso [scale_colour_discrete_epi()], [scale_colour_continuous_epi()]
#'
#' @importFrom ggplot2 scale_fill_manual
#' @export
scale_fill_discrete_epi <- function(palette = epi_palette_discrete, ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "scale_fill_discrete_epi()",
    "scale_fill_discrete()"
  )
  structure(list(
    scale_fill_manual(values = palette, ...)
  ))
}

#' Colour scale to fill geoms using EPI colours
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by the more explicit [scale_fill_discrete_epi()] and [scale_fill_continuous_epi()],
#' which can be used in the same fashion. It will be removed in the next version.
#'
#' @param palette A list of colours to use
#' @param ... Arguments passed to scale_fill_manual
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   scale_fill_epi()
#' }
#' # ->
#' \dontrun{
#' ggplot(...) +
#'   scale_fill_discrete_epi()
#' }
#' @keywords internal
scale_fill_epi <- function(palette = epi_palette_discrete, ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "scale_fill_epi()",
    "scale_fill_discrete_epi()"
  )
  structure(list(
    ggplot2::scale_fill_manual(values = palette, ...)
  ))
}

#' Colour scale for discrete data using EPI colours
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by the simpler [epitheme::scale_colour_discrete())]
#' which can be used in the same fashion. It will be removed in the next version.
#'
#' @param palette A list of colours to use
#' @param ... Arguments passed to scale_colour_manual
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   scale_colour_discrete_epi()
#' }
#' @seealso [scale_fill_discrete_epi()], [scale_colour_continuous_epi()]
#'
#' @importFrom ggplot2 scale_colour_manual
#' @export
scale_colour_discrete_epi <- function(palette = epi_palette_discrete, ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "scale_colour_discrete_epi()",
    "scale_colour_discrete()"
  )
  structure(list(
    scale_colour_manual(values = palette, ...)
  ))
}

#' Colour scale for continuous data using EPI colours
#'
#' @description
#' `r lifecycle::badge("deprecated")`
#'
#' This function has been superseded by the simpler [epitheme::scale_colour_continuous())]
#' which can be used in the same fashion. It will be removed in the next version.
#'
#' @param palette A list of colours to use, default is sequential 6-class palette
#' @param ... Arguments passed to scale_colour_gradientn
#' @examples
#' \dontrun{
#' ggplot(...) +
#'   scale_colour_continuous_epi()
#' }
#' @seealso [scale_colour_discrete_epi()], [scale_fill_discrete_epi()]
#'
#' @importFrom ggplot2 scale_colour_gradientn
#' @export
scale_colour_continuous_epi <- function(palette = epi_palette_seq6, ...) {
  lifecycle::deprecate_warn(
    "0.4.0",
    "scale_colour_continuous_epi()",
    "scale_colour_continuous()"
  )
  structure(list(
    scale_colour_gradientn(colours = palette, ...)
  ))
}

#' EPI scale_colour_discrete
#' @param ... arguments passed to scale_fill_manual
#' @param values colour values
#' @importFrom ggplot2 scale_colour_manual
#' @export
scale_colour_discrete <- function(...,
                                  values = epi_palette_discrete) {
  scale_colour_manual(..., values = values)
}

#' EPI scale_fill_discrete
#' @param ... arguments passed to scale_fill_manual
#' @param values colour values
#' @importFrom ggplot2 scale_fill_manual
#' @export
scale_fill_discrete <- function(...,
                                values = epi_palette_discrete) {
  scale_fill_manual(..., values = values)
}

#' EPI scale_colour_continuous
#' @param ... arguments passed to scale_colour_gradientn
#' @param colours colours
#' @importFrom ggplot2 scale_colour_gradientn
#' @export
scale_colour_continuous <- function(...,
                                    colours = epi_palette_seq6) {
  scale_colour_gradientn(..., colours = colours)
}

#' EPI scale_fill_continuous
#' @param ... arguments passed to scale_fill_gradient
#' @param low low colour
#' @param high high colour
#' @importFrom ggplot2 scale_fill_gradient
#' @export
scale_fill_continuous <- function(...,
                                  low = "#F5F5F5",
                                  high = epi_corp_colour) {
  scale_fill_gradient(..., low = low, high = high)
}
